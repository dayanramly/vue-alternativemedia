import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/components/homepage/Layout'
import Purchase from '@/components/purchase/Layout'
import NotFound from '@/components/notfound/Main'
import { bus } from '../main'
import { store } from '../store/store'

Vue.use(Router)

export default new Router({
	mode: 'history',
	scrollBehavior (to, from, savedPosition) {
		return savedPosition || {x: 0, y: 100}
	},
	routes: [
	{
		path: '/',
		name: 'HomeLayout',
		component: Homepage
	},
	{
		path: '/purchase',
		name: 'PurchaseLayout',
		component: Purchase,
		children: [
		{
			path: '/purchase/step2',
			name: 'step2',
			component: () => import('@/components/purchase/step/step2'),
			beforeEnter: (to, from, next) => {
				next();
			}
		},
		{
			path: '/purchase/step3',
			name: 'step3',
			meta: {
				title: "Ok, you want to buy a <strong>home or move</strong> – that’s exciting.",
				subtitle: "",
				step: 3
			},
			component: () => import('@/components/purchase/step/step3'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step4',
			name: 'step4',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 4
			},
			component: () => import('@/components/purchase/step/step4'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step5',
			name: 'step5',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 5
			},
			component: () => import('@/components/purchase/step/step5'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step6',
			name: 'step6',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 6
			},
			component: () => import('@/components/purchase/step/step6'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step7',
			name: 'step7',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 7
			},
			component: () => import('@/components/purchase/step/step7'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step8',
			name: 'step8',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 8
			},
			component: () => import('@/components/purchase/step/step8'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step9',
			name: 'step9',
			meta: {
				title: "Ok, you want to buy a <strong>home or move</strong> – that’s exciting.",
				subtitle: "",
				step: 9
			},
			component: () => import('@/components/purchase/step/step9'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step10',
			name: 'step10',
			meta: {
				title: "Ok, you want to buy a <strong>home or move</strong> – that’s exciting.",
				subtitle: "",
				step: 10
			},
			component: () => import('@/components/purchase/step/step10'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step11',
			name: 'step11',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 11
			},
			component: () => import('@/components/purchase/step/step11'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step12',
			name: 'step12',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 12
			},
			component: () => import('@/components/purchase/step/step12'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step13',
			name: 'step13',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 13
			},
			component: () => import('@/components/purchase/step/step13'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},      	{
			path: '/purchase/step14',
			name: 'step14',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 14
			},
			component: () => import('@/components/purchase/step/step14'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step15',
			name: 'step15',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 15
			},
			component: () => import('@/components/purchase/step/step15'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step16',
			name: 'step16',
			meta: {
				title: "Let’s start by calculating how much you need to borrow",
				subtitle: "",
				step: 16
			},
			component: () => import('@/components/purchase/step/step16'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step17',
			name: 'step17',
			meta: {
				title: "Congratulations, looks like you <strong>qualify</strong> for some great <strong>home loan</strong> deals.",
				subtitle: "Before we can provide your personalised borrowing power report, we'll need to grab few more details.",
				step: 17
			},
			component: () => import('@/components/purchase/step/step17'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle)
				next();
			}
		},
		{
			path: '/purchase/step18',
			name: 'step18',
			meta: {
				title: "Now check your <strong>phone.</strong>",
				subtitle: "",
				step: 16
			},
			component: () => import('@/components/purchase/step/step18'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{
			path: '/purchase/step19',
			name: 'step19',
			meta: {
				title: "Thank you...",
				subtitle: "Thanks for leaving those details - we may give you a quick ring to ask a few extra questions to provide you with other home loan options that may be relevant to you.",
				step: 18
			},
			component: () => import('@/components/purchase/step/step19'),
			beforeEnter: (to, from, next) => {
				bus.$emit('changeTitle', to.meta.title);
				bus.$emit('changeSubTitle', to.meta.subtitle);
				next();
			}
		},
		{ 
			path: '/purchase/*', 
			redirect: '/'
		}
		]
	},
	{ path: '*', redirect: '/' },
	]
})
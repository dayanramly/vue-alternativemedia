import cities from '../../static/js/geodata.json'

const cityData = cities.map((item) => {
	const container = {
		'city' : item[0],
		'code' : item[1],
		'postal' : item[2]
	};
	return container
});

export function ajaxFindCountry (query) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
    	const results = cityData.filter((element, index, array) => {
    		return (element.city.toLowerCase() || element.code.toLowerCase()).includes(query.toLowerCase()) || (element.postal.includes(query))
    	})
      resolve(results)
    }, 1000)
  })
}
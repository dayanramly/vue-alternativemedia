import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		havePartner : true,
		cityData : {}
	},
	actions: {
		loadCity ({commit}){
			axios
			.get('/static/js/geodata.json')
			.then(response => {
				let cityData = response.data.map((item) => {
					const container = {
						'city' : item[0],
						'code' : item[1],
						'postal' : item[2]
					};
					return container
				});
				commit('Set_City', cityData)
			})
			.catch(error => {
				console.log(error)
			})
		}
	},
	mutations:{
		changePartnerStatus: state => {
			state.havePartner = false
		}, 
		Set_City: (state, cityData) => {
			state.cityData = cityData
		}
	}
});
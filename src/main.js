// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Vuex from 'vuex'
import router from './router'
import VeeValidate from 'vee-validate'
import BootstrapVue from 'bootstrap-vue'
import money from 'v-money'
import VueNumeric from 'vue-numeric'
import './custom.scss'
import { store } from './store/store'



Vue.use(BootstrapVue)
Vue.use(money, {precision: 4})
Vue.use(VueNumeric)
Vue.use(VeeValidate, {
  classes: true,
  classNames: {
    valid: 'is-valid',
    invalid: 'is-invalid'
  }
})
Vue.config.productionTip = false

export const bus = new Vue()

/* eslint-disable no-new */
new Vue({
  store: store,
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  created() {
    this.$store.dispatch('loadCity')
  }
})
